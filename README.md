﻿
1. Clone both rentgraph-api and rentgraph-client projects

2. Make sure you have Gradle (e.g. gradle-5.2.1) installed 
with appropriate PATH system environment variable in place

3. Make sure you have nodejs installed


4. Make sure you have PostgreSQL installed with an administration platform (e.g. pgAdmin 4)

during set-up/installation:
- set up password as "tere" 
or change the corresponding line in "./rentgraph-api/src/main/resources/application.properties" with your chosen password
- set port to 5432 
or change corresponding line in the application.properties file mentioned above

5. Open git bash or terminal of choice in path of rentgraph-client folder, run commands "npm install" and "npm start"

6. Open PostgreSQL admin platform.
In pgAdmin 4:
right click on "Databases", 
create new database named "rentgraph", 
right-click on it, choose "Restore..." from dropdown menu, 
click on "..." from right end of "Filename" bar and navigate to "Final_rentgraphSQL_BACKUP.tar", located in path:
"./rentgraph-api/src/main/resources/"
Make sure to have "All Files" selected from the "Format" drop-down menu.
Click "Select". Click "Restore".

7. Build and run rentgraph-api (in IntelliJ: from right sidemenu "Gradle").

8. Open "http://localhost:3000/" url using browser of choice, enter "a" as username and "a" as passwor. Click on "Log in".

In the current state there are still pretty significant bugs in element rendering, so would be a good idea to refresh the page when "all tables" are hidden.




