package com.rentgraph.rentgraphapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthCheck {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @GetMapping("/colors")
    public Colors getColors() {

        int greenCount = jdbcTemplate.queryForObject("select count(*) from contract where (end_date > current_timestamp + interval '90 days') and active = 1", Integer.class);
        int yellowCount = jdbcTemplate.queryForObject("select count(*) from contract where (end_date > current_timestamp) and (end_date < current_timestamp + interval '90 days') and active = 1", Integer.class);
        int redCount = jdbcTemplate.queryForObject("select count(*) from contract where end_date < current_timestamp and active = 1", Integer.class);
        int whiteCount = jdbcTemplate.queryForObject("select count(*) from rental where not exists (select contract.id from contract where contract.rental_id = rental.id)", Integer.class);

        //where start_date < current_timestamp and

        Colors colors = new Colors();

        colors.setGreen(greenCount);
        colors.setRed(redCount);
        colors.setYellow(yellowCount);
        colors.setWhite(whiteCount);


        return colors;
    }






}