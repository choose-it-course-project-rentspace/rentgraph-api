package com.rentgraph.rentgraphapi;

// happab olema andmekandja
public class Rental {


    private int id;
    private String address;
    private String logo;

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}