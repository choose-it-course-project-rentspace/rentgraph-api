package com.rentgraph.rentgraphapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addUser(String username, String password) {
        jdbcTemplate.update("insert into users (username, password) values (?, ?)", username, password);
    }

    public User getUser(String username) {
        return jdbcTemplate.queryForObject("select * from users where username = ?",
                new Object[]{username},
                (row, count) -> {
                    User user = new User();
                    user.setId(row.getInt("id"));
                    user.setUsername(row.getString("username"));
                    user.setPassword(row.getString("password"));
                    return user;
                });
    }
}

