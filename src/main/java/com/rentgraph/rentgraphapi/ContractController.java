package com.rentgraph.rentgraphapi;

import com.rentgraph.rentgraphapi.Rental;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class ContractController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting2")
    public String getGreeting2() {
        return "Tere2!!!";
    }



    @GetMapping("/contracts")
    public List <Contract> getContracts() {

        List <Contract> contracts = new ArrayList<>();

        contracts = jdbcTemplate.query("select * from contract", (row, count) -> {


                    int contractId = row.getInt("id");
                    Date contractStartDate = row.getDate("start_date");
                    Date contractEndDate = row.getDate("end_date");
                    int contractRentalId = row.getInt("rental_id");
                    int contractTenantId = row.getInt("tenant_id");
                    int contractActive = row.getInt("active");

                    Contract contract = new Contract();
                    contract.setId(contractId);
                    contract.setStartDate(contractStartDate);
                    contract.setEndDate(contractEndDate);
                    contract.setRentalId(contractRentalId);
                    contract.setTenantId(contractTenantId);
                    contract.setActive(contractActive);

                    return contract;

                }
        );

        return contracts;

    }


    //

    public List<Rental> getRentals() {
        List<Rental> rentals = jdbcTemplate.query("select * from rental", (row, count) -> {
                    int rentalId = row.getInt("id");
                    String rentalAddress = row.getString("address");
                    String rentalLogo = row.getString("logo");

                    Rental rental = new Rental();
                    rental.setId(rentalId);
                    rental.setAddress(rentalAddress);
                    rental.setLogo(rentalLogo);

                    return rental;
                }
        );

        return rentals;
    }


    @GetMapping("/contract/{id}")
    public Contract getContract(@PathVariable int id) {
        Contract result = jdbcTemplate.queryForObject("select * from contract where id = ?", new Object[]{id}, (row, count) -> {
            int contractId = row.getInt("id");
            Date contractStartDate = row.getDate("start_date");
            Date contractEndDate = row.getDate("end_date");
            int contractRentalId = row.getInt("rental_id");
            int contractTenantId = row.getInt("tenant_id");
            int contractActive = row.getInt("active");

            Contract contract = new Contract();
            contract.setId(contractId);
            contract.setStartDate(contractStartDate);
            contract.setEndDate(contractEndDate);
            contract.setRentalId(contractRentalId);
            contract.setTenantId(contractTenantId);
            contract.setActive(contractActive);

            return contract;
        });

        return result;
    }







    @DeleteMapping("/contract/{id}")
    public void deleteRental(@PathVariable int id) {
        jdbcTemplate.update("delete from contract where id = ?", id);
    }

    @PostMapping("/contract")
    public void addContract(@RequestBody Contract contract) {
        jdbcTemplate.update("insert into contract (start_date, end_date, tenant_id, rental_id, active) value (?, ?, ?, ?, ?)",
                contract.getStartDate(), contract.getEndDate(), contract.getTenantId(), contract.getRentalId(), contract.getActive());
    }

    @PostMapping("/contracts")
    public void addContracts(@RequestBody Contract[] contracts) {
        for (Contract contract : contracts) {
            addContract(contract);
        }
    }

    @PutMapping("/contract")
    public void editContract(@RequestBody Contract contract) {
        jdbcTemplate.update("update contract set start_date = ?, end_date = ?, tenant_id = ?, rental_id = ?, active = ?, where id = ?",
                contract.getStartDate(), contract.getEndDate(), contract.getTenantId(), contract.getRentalId(), contract.getActive(), contract.getId());
    }

}
