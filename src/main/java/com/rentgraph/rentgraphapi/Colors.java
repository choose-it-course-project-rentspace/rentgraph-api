package com.rentgraph.rentgraphapi;

public class Colors {

    private long red;
    private long yellow;
    private long green;
    private long white;

    public long getRed() {
        return red;
    }

    public void setRed(long red) {
        this.red = red;
    }

    public long getYellow() {
        return yellow;
    }

    public void setYellow(long yellow) {
        this.yellow = yellow;
    }

    public long getGreen() {
        return green;
    }

    public void setGreen(long green) {
        this.green = green;
    }


    public long getWhite() {
        return white;
    }

    public void setWhite(long white) {
        this.white = white;
    }
}

