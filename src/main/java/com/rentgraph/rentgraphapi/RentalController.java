package com.rentgraph.rentgraphapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class RentalController {

    @Autowired
    private JdbcTemplate jdbcTemplate;




    @GetMapping("/rentals/{color}")
    public List <Relevants> getColorRentals(@PathVariable int color) {

        List <Relevants> colorRentals = new ArrayList<>();


        if (color == 0) {
            return this.getRelevants();
        }

        if (color == 1) {
            colorRentals = jdbcTemplate.query("select *, " +
                    "(select contract.end_date from contract " +
                    "where contract.rental_id = rental.id and contract.active = 1 order by end_date limit 1) " +
                    "as contract_end_date, " +
                    "(select tenant.name from tenant " +
                    "inner join contract on contract.tenant_id = tenant.id and contract.rental_id = rental.id and contract.active = 1 limit 1) " +
                    "as tenant_name " +
                    "from rental where id in (select distinct rental_id from contract " +
                    "where ((end_date > (current_timestamp + interval '90 days')) and (active = 1)))", (row, count) -> {


                String rentalAddress = row.getString("address");
                Date endDate = row.getDate("contract_end_date");
                String tenants = row.getString("tenant_name");

                        Relevants relevants = new Relevants();
                        relevants.setAddress(rentalAddress);
                        relevants.setEndDate(endDate);
                        relevants.setTenants(tenants);

                        return relevants;

            }
            );

            return colorRentals;

        }
        else if (color == 2) {
            colorRentals = jdbcTemplate.query("select *, " +
                    "(select contract.end_date from contract " +
                    "where contract.rental_id = rental.id and contract.active = 1 order by end_date limit 1) " +
                    "as contract_end_date, " +
                    "(select tenant.name from tenant " +
                    "inner join contract on contract.tenant_id = tenant.id and contract.rental_id = rental.id and contract.active = 1 limit 1) " +
                    "as tenant_name " +
                    "from rental where id in (select distinct rental_id from contract " +
                    "where ((end_date > current_timestamp) and (end_date < (current_timestamp + interval '90 days')) and (active = 1)))", (row, count) -> {


                        String rentalAddress = row.getString("address");
                        Date endDate = row.getDate("contract_end_date");
                        String tenants = row.getString("tenant_name");

                        Relevants relevants = new Relevants();
                        relevants.setAddress(rentalAddress);
                        relevants.setEndDate(endDate);
                        relevants.setTenants(tenants);

                        return relevants;

                    }
            );


            return colorRentals;

        }else if (color == 3) {
            colorRentals = jdbcTemplate.query("select *, " +
                    "(select contract.end_date from contract " +
                    "where contract.rental_id = rental.id and contract.active = 1 order by end_date limit 1) " +
                    "as contract_end_date, " +
                    "(select tenant.name from tenant " +
                    "inner join contract on contract.tenant_id = tenant.id and contract.rental_id = rental.id and contract.active = 1 limit 1) " +
                    "as tenant_name " +
                    "from rental where id in (select distinct rental_id from contract " +
                    "where ((end_date < current_timestamp) and (active = 1)))", (row, count) -> {


                        String rentalAddress = row.getString("address");
                        Date endDate = row.getDate("contract_end_date");
                        String tenants = row.getString("tenant_name");

                        Relevants relevants = new Relevants();
                        relevants.setAddress(rentalAddress);
                        relevants.setEndDate(endDate);
                        relevants.setTenants(tenants);

                        return relevants;

                    }
            );

            return colorRentals;

        }else if (color == 4) {
            colorRentals = jdbcTemplate.query("select * from rental " +
                    "where not exists " +
                    "(select contract.id from contract where contract.rental_id = rental.id)", (row, count) -> {


                        String rentalAddress = row.getString("address");
//                        Date endDate = row.getDate("contract_end_date");
//                        String tenants = row.getString("tenant_name");

                        Relevants relevants = new Relevants();
                        relevants.setAddress(rentalAddress);
//                        relevants.setEndDate(endDate);
//                        relevants.setTenants(tenants);

                        return relevants;

                    }
            );

            return colorRentals;

        }


        return colorRentals;
    }




    public List<Relevants> getRelevants() {
        List<Relevants> relevantRentals = jdbcTemplate.query("select " +
                "rental.*, " +
                "\tcontract.end_date as contract_end_date, " +
                "tenant.name as tenant_name " +
                "from rental " +
                "inner join contract on contract.rental_id = rental.id and contract.active = 1 " +
                "left join tenant on contract.tenant_id = tenant.id"
                , (row, count) -> {
                    String rentalAddress = row.getString("address");
                    Date endDate = row.getDate("contract_end_date");
                    String tenants = row.getString("tenant_name");

                    Relevants relevants = new Relevants();
                    relevants.setAddress(rentalAddress);
                    relevants.setEndDate(endDate);
                    relevants.setTenants(tenants);

                    return relevants;

                }
        );

        return relevantRentals;

    }


//    Varasem ver:
//    "select *, " +
//            "(select contract.end_date from contract " +
//            "where contract.rental_id = rental.id and contract.active = 1 order by end_date limit 1) " +
//            "as contract_end_date, " +
//            "(select tenant.name from tenant " +
//            "inner join contract on contract.tenant_id = tenant.id and contract.rental_id = rental.id and contract.active = 1 limit 1) " +
//            "as tenant_name " +
//            "from rental"





    @PostMapping("/relevant")
    public void addRelevants(@RequestBody Relevants relevants) {
        jdbcTemplate.update("insert into tenant (name) values (?)", relevants.getTenants());
        int tenantId = this.jdbcTemplate.queryForObject( "select max(id) from tenant" , Integer.class);

        jdbcTemplate.update("insert into rental (address) values (?)", relevants.getAddress());
        int rentalId = this.jdbcTemplate.queryForObject( "select max(id) from rental", Integer.class );
//        int rentalId = this.jdbcTemplate.queryForObject( "select @@identity", Integer.class );
        int activeInt = 1;

        jdbcTemplate.update("insert into contract (end_date, tenant_id, rental_id, active) values (?, ?, ?, ?)", relevants.getEndDate(), tenantId, rentalId, activeInt);

    }



//
//    @PutMapping("/relevant")
//    public void editRelevants(@RequestBody Relevants relevants) {
//        jdbcTemplate.update("update rental set address = ?"), relevants.getAddress());
//        jdbcTemplate.update("update contract set end_date = ?"), relevants.getEndDate());
//        jdbcTemplate.update("update tenant set name = ? where id = ?"), relevants.getTenants());
//
//        relevants.getAddress(), relevants.getTenants(), relevants.getEndDate()); // LISADA VIIMASE ? ASEMEL INDEKSI???? KAS VAJA??
//    }


    @GetMapping("/rentals")
    public List<Rental> getRentals() {
        List<Rental> rentals = jdbcTemplate.query("select * from rental", (row, count) -> {
                    int rentalId = row.getInt("id");
                    String rentalAddress = row.getString("address");
                    String rentalLogo = row.getString("logo");

                    Rental rental = new Rental();
                    rental.setId(rentalId);
                    rental.setAddress(rentalAddress);
                    rental.setLogo(rentalLogo);

                    return rental;
                }
        );

        return rentals;
    }



    @GetMapping("/rental/{id}")
    public Rental getRental(@PathVariable int id) {
        Rental result = jdbcTemplate.queryForObject("select * from rental where id = ?", new Object[]{id}, (row, count) -> {
            int rentalId = row.getInt("id");
            String rentalAddress = row.getString("address");
            String rentalLogo = row.getString("logo");

            Rental rental = new Rental();
            rental.setAddress(rentalAddress);


            return rental;
        });

        return result;
    }







    @DeleteMapping("/rental/{id}")
    public void deleteRental(@PathVariable int id) {
        jdbcTemplate.update("delete from rental where id = ?", id);
    }



    @PostMapping("/rentals")
    public void addRentals(@RequestBody Rental[] rentals) {
        for (Rental rental : rentals) {
            addRental(rental);
        }
    }




    @PostMapping("/rental")
    public void addRental(@RequestBody Rental rental) {
        jdbcTemplate.update("insert into rental (address, logo) value (?, ?)", rental.getAddress(), rental.getLogo());
    }

    @PutMapping("/rental")
    public void editRental(@RequestBody Rental rental) {
        jdbcTemplate.update("update rental set address = ?, logo = ? where id = ?",
                rental.getAddress(), rental.getLogo(), rental.getId());
    }










//
//
//    @PutMapping("/relevant")
//    public void editRelevants(@RequestBody Relevants relevants) {
//        jdbcTemplate.update("update rental set address = ? update tenants set name = ?, update contract set end_date = ? where id = ?",
//                relevants.getAddress(), relevants.getTenants(), relevants.getEndDate()); // LISADA VIIMASE ? ASEMEL INDEKSI???? KAS VAJA??
//    }



}
