package com.rentgraph.rentgraphapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class TenantController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting3")
    public String getGreeting3() {
        return "Tere3!!!";
    }


    @GetMapping("/tenants")
    public List<Tenant> getTenants() {

        List<Tenant> tenants = jdbcTemplate.query("select * from tenant", (row, count) -> {
                    int tenantId = row.getInt("id");
                    String tenantName = row.getString("name");
                    String tenantEmail = row.getString("email");
                    String tenantPhone = row.getString("phone");
                    String tenantPersonalId = row.getString("personal_id");
                    String tenantAddress = row.getString("address");

                    Tenant tenant = new Tenant();
                    tenant.setId(tenantId);
                    tenant.setName(tenantName);
                    tenant.setEmail(tenantEmail);
                    tenant.setPhone(tenantPhone);
                    tenant.setPersonalId(tenantPersonalId);
                    tenant.setAddress(tenantAddress);

                    return tenant;
                }
        );

        return tenants;
    }

    @GetMapping("/tenant/{id}")
    public Tenant getTenant(@PathVariable int id) {
        Tenant result = jdbcTemplate.queryForObject("select * from tenant where id = ?", new Object[]{id}, (row, count) -> {
            int tenantId = row.getInt("id");
            String tenantName = row.getString("name");
            String tenantEmail = row.getString("email");
            String tenantPhone = row.getString("phone");
            String tenantPersonalId = row.getString("personal_id");
            String tenantAddress = row.getString("address");

            Tenant tenant = new Tenant();
            tenant.setId(tenantId);
            tenant.setName(tenantName);
            tenant.setEmail(tenantEmail);
            tenant.setPhone(tenantPhone);
            tenant.setPersonalId(tenantPersonalId);
            tenant.setAddress(tenantAddress);

            return tenant;
        });

        return result;
    }


    @DeleteMapping("/tenant/{id}")
    public void deleteTenant(@PathVariable int id) {
        jdbcTemplate.update("delete from tenant where id = ?", id);
    }

    @PostMapping("/tenant")
    public void addTenant(@RequestBody Tenant tenant) {
        jdbcTemplate.update("insert into tenant (name, email, phone, personal_id, address) value (?, ?, ?, ?, ?)",
                tenant.getName(), tenant.getEmail(), tenant.getPhone(), tenant.getPersonalId(), tenant.getAddress());
    }

    @PostMapping("/tenants")
    public void addTenants(@RequestBody Tenant[] tenants) {
        for (Tenant tenant : tenants) {
            addTenant(tenant);
        }
    }

    @PutMapping("/tenant")
    public void editTenant(@RequestBody Tenant tenant) {
        jdbcTemplate.update("update tenant set name = ?, email = ?, phone = ?, personal_id = ?, address = ? where id = ?",
                tenant.getName(), tenant.getEmail(), tenant.getPhone(), tenant.getPersonalId(), tenant.getAddress(), tenant.getId());
    }

}
