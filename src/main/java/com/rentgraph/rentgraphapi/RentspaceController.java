package com.rentgraph.rentgraphapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class RentspaceController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting4")
    public String getGreeting4() {
        return "Tere4!!!";
    }


    @GetMapping("/rentspaces")
    public List<Rentspace> getRentspaces() {
        List<Rentspace> rentspaces = jdbcTemplate.query("select * from rental", (row, count) -> {
                    int rentspaceId = row.getInt("id");
                    String rentspaceAddress = row.getString("address");
                    String rentspaceLogo = row.getString("logo");

                    Rentspace rentspace = new Rentspace();
                    rentspace.setId(rentspaceId);
                    rentspace.setAddress(rentspaceAddress);
                    rentspace.setLogo(rentspaceLogo);

                    return rentspace;
                }
        );

        return rentspaces;
    }



    @GetMapping("/rentspace/{id}")
    public Rentspace getRentspace(@PathVariable int id) {
        Rentspace result = jdbcTemplate.queryForObject("select * from rental where id = ?", new Object[]{id}, (row, count) -> {
            int rentspaceId = row.getInt("id");
            String rentspaceAddress = row.getString("address");
            String rentspaceLogo = row.getString("logo");

            Rentspace rentspace = new Rentspace();
            rentspace.setAddress(rentspaceAddress);


            return rentspace;
        });

        return result;
    }







    @DeleteMapping("/rentspace/{id}")
    public void deleteRentspace(@PathVariable int id) {
        jdbcTemplate.update("delete from rental where id = ?", id);
    }



    @PostMapping("/rentspaces")
    public void addRentspaces(@RequestBody Rentspace[] rentspaces) {
        for (Rentspace rentspace : rentspaces) {
            addRentspace(rentspace);
        }
    }


    @PostMapping("/rentspace")
    public void addRentspace(@RequestBody Rentspace rentspace) {
        jdbcTemplate.update("insert into rental (address, logo) values (?, ?)", rentspace.getAddress(), rentspace.getLogo());
    }

    @PutMapping("/rentspace")
    public void editRentspace(@RequestBody Rentspace rentspace) {
        jdbcTemplate.update("update rental set address = ?, logo = ? where id = ?",
                rentspace.getAddress(), rentspace.getLogo(), rentspace.getId());
    }


}
